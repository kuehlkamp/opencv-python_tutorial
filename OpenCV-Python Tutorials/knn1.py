import cv2
import numpy as np
import matplotlib.pyplot as plt

# generate a feature set containing (x, y) values of 25 known training data
trainData = np.random.randint(0,100,(25,2)).astype(np.float32)

# Label each one either red or blue with numbers 0 and 1
responses = np.random.randint(0,2,(25,1)).astype(np.float32)

# plot read families
red = trainData[responses.ravel() == 0]
plt.scatter(red[:,0],red[:,1], 80, 'r', '^')

# plot blue families
blue = trainData[responses.ravel() == 1]
plt.scatter(blue[:,0],blue[:,1], 80, 'b', 's')

newcomer = np.random.randint(0,100,(1,2)).astype(np.float32)
plt.scatter(newcomer[:,0],newcomer[:,1], 80,'g','o')

knn = cv2.KNearest()
knn.train(trainData, responses)
ret, results, neighbors, dist = knn.find_nearest(newcomer, 3)

print("result: ", results)
print("neighbors: ", neighbors)
print("distance: ", dist)

plt.show()