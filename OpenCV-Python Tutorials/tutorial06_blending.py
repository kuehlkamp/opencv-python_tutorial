# https://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_imgproc/py_pyramids/py_pyramids.html#pyramids

import cv2
import numpy as np

A = cv2.imread('image1.jpg')
B = cv2.imread('image2.jpg')

# generate gaussian pyramid for A
G = A.copy()
gpA = [G]
for i in xrange(6):
    G = cv2.pyrDown(G)
    gpA.append(G)
    
# generate Gaussian pyramid for B
G = B.copy()
gpB = [G]
for i in xrange(6):
    G = cv2.pyrDown(G)
    gpB.append(G)
    #cv2.imshow('gaussian pyramid',G)
    #cv2.waitKey(0)

# generate Laplacian pyramid for A
lpA = [gpA[5]]
for i in xrange(5,0,-1):
    GE = cv2.pyrUp(gpA[i])
    rows, cols, dpt = gpA[i-1].shape
    GE = cv2.resize(GE, (cols, rows))
    L = cv2.subtract(gpA[i-1],GE)
    lpA.append(L)
    #cv2.imshow('Laplacian pyramid',L)
    #cv2.waitKey(0)

# generate Laplacian pyramid for B
lpB = [gpB[5]]
for i in xrange(5,0,-1):
    GE = cv2.pyrUp(gpB[i])
    rows, cols, dpt = gpB[i-1].shape
    GE = cv2.resize(GE, (cols, rows))
    L = cv2.subtract(gpB[i-1],GE)
    lpB.append(L)
    #cv2.imshow('Laplacian pyramid',L)
    #cv2.waitKey(0)

# add left and right halves of images in each level
LS = []
for la, lb in zip(lpA, lpB):
    rows, cols, dpt = la.shape
    ls = np.hstack((la[:,0:cols/2], lb[:,cols/2:]))
    LS.append(ls)
#    cv2.imshow('debug',ls)
#    cv2.waitKey(0)

# now reconstruct
ls_ = LS[0]
for i in xrange(1,6):
    ls_ = cv2.pyrUp(ls_)
    rows, cols, dpt = LS[i].shape
    ls_ = cv2.resize(ls_, (cols, rows))
    ls_ = cv2.add(ls_, LS[i])
    #cv2.imshow('Reconstruction',ls_)
    #cv2.waitKey(0)

# image with direct connecting each half
real = np.hstack((A[:,:cols/2],B[:,cols/2:]))

cv2.imwrite('Pyramid_blending2.jpg',ls_)
cv2.imwrite('Direct_blending.jpg',real)
