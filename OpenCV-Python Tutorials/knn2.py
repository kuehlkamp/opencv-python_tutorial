import numpy as np
import cv2
from matplotlib import pyplot as plt
import os

datafile='knn_data.npz'
# initiate KNN
knn = cv2.KNearest()

if (os.path.isfile(datafile)):
    with np.load(datafile) as data:
        train = data['train']
        train_labels = data['train_labels']
        test = data['test']
        test_labels = data['test_labels']
else:
    gray = cv2.imread('digits.png', 0)

    # split the image into 5000 cells of 20x20 size
    cells = [np.hsplit(row, 100) for row in np.vsplit(gray, 50)]
    # make it into a numpy array (50, 100, 20, 20)
    x = np.array(cells)

    # prepare train_data and test_data
    train = x[:,:50].reshape(-1,400).astype(np.float32)  # size (2500,400) - each line contains a digit
    test = x[:,50:].reshape(-1,400).astype(np.float32)  # same goes here

    # create labels for train and test
    k = np.arange(10)
    train_labels = np.repeat(k,250)[:,np.newaxis]
    test_labels = train_labels.copy()

    # save the training
    np.savez('knn_data.npz', train=train, train_labels=train_labels, test=test, test_labels=test_labels)

# train the data 
knn.train(train, train_labels)

# test it for k=5
ret, result, neighbors, dist = knn.find_nearest(test,k=5)

# check the accuracy of classification
matches = result==test_labels
correct = np.count_nonzero(matches)
accuracy = correct*100.0/result.size
print(accuracy)