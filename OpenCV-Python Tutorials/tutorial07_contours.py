# based upon https://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_imgproc/py_contours/py_contour_features/py_contour_features.html

import cv2
import numpy as np

# find contours
img = cv2.imread('fish.jpg', 0)
retval, thresh = cv2.threshold(img, 202, 255, cv2.THRESH_BINARY)
thresh = cv2.bitwise_not(thresh)  # invert the image
_, contours, hierarchy = cv2.findContours(thresh, 1, 2)

# calculate moments
for cnt in contours:
    M = cv2.moments(cnt)
    #print(M)

    # calculate centroid]
    if M['m00'] != 0:
        cx = int(M['m10']/M['m00'])
        cy = int(M['m01']/M['m00'])

    # get area
    area = cv2.contourArea(cnt)
    # area = M['m00']

    # get perimeter
    perimeter = cv2.arcLength(cnt, True)

    # approximate a polygon to the contour
    cnt_len = cv2.arcLength(cnt, True)
    cnt = cv2.approxPolyDP(cnt, 0.02*cnt_len, True)

    cv2.drawContours(img, cnt, -1, (255,255,255), 3)


cv2.imshow('contours',img)
cv2.waitKey(0)
