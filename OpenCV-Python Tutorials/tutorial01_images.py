import numpy as np
import cv2
from matplotlib import pyplot as plt

# read image (grayscale)
img = cv2.imread('baboon.jpg')
# display
cv2.imshow('image',img)
cv2.waitKey(0)
# write the image
cv2.imwrite('baboongray.png',img)
cv2.destroyAllWindows()

# convert RGB to BGR (with numpy indexing)
img2 = img[:,:,::-1] 
# display using matplotlib instead
plt.imshow(img2, cmap='gray', interpolation='bicubic')
plt.xticks([]), plt.yticks([]) # hide tick values
plt.show()

