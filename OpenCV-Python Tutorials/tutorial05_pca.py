import numpy as np
import cv2
import sys

# PCA utilization in OpenCV
# based on http://www.bytefish.de/blog/pca_in_opencv/

def norm_0_255(src):
    dst = None
    dst = cv2.normalize(src, dst, 0, 255, cv2.NORM_MINMAX, cv2.CV_8UC1)
    return dst

def asRowMatrix(src, alpha = 1, beta = 0):
    """
    Converts the images passed in src into a row matrix
    """
    n = len(src)
    if n == 0:
        return  None
    # create a resulting matrix
    data = np.array([im.reshape(im.size) for im in src], np.float32)
    return data


def main():
    # load the images of ATT face database
    basedir = 'attfaces'
    folders = ['s1','s2','s3','s4','s5','s6','s7','s8','s9']
    files = range(1,11)
    db = []
    for f in folders:
        for file in files:
            path = '{}/{}/{}.pgm'.format(basedir, f, file)
            print('Loading ', path)
            im = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
            db.append(im)

    # store the size of a single image
    sz = np.shape(db[0])

    # build a matrix with each image in a row 
    data = asRowMatrix(db)

    # number of components to keep for the PCA
    num_components = 10.

    # perform PCA
    # clarification about the reshape: http://stackoverflow.com/questions/8567704/opencv-pca-compute-in-python
    mean, eigenvecs = cv2.PCACompute(data, np.mean(data, axis=0).reshape(1,-1) )
    
    # the mean face
    cv2.imshow('avg', norm_0_255(mean.reshape(sz)))

    # the first three eigenfaces
    cv2.imshow('pc1', norm_0_255(eigenvecs[0].reshape(sz)))
    cv2.imshow('pc2', norm_0_255(eigenvecs[1].reshape(sz)))
    cv2.imshow('pc3', norm_0_255(eigenvecs[2].reshape(sz)))

    cv2.waitKey(0)


if __name__ == "__main__":
    sys.exit(int(main() or 0))